package wantsome.budget_tracker.database.init;

import wantsome.budget_tracker.business.CategoryService;
import wantsome.budget_tracker.business.EntryService;
import wantsome.budget_tracker.database.ddl.CategoryDdl;
import wantsome.budget_tracker.database.ddl.EntryDdl;

public class DbInit {
    public static EntryService entryService = EntryService.getInstance();
    public static CategoryService categoryService = CategoryService.getInstance();

    public static void initializeDemoDb() {
        EntryDdl.dropTable();
        CategoryDdl.dropTable();
        CategoryDdl.createTable();
        EntryDdl.createTable();

        String basePath = "src/main/java/wantsome/budget_tracker/database/init/";
        String categoriesInputs = basePath + "categories.csv";
        categoryService.loadFromCsvFile(categoriesInputs);

        String entriesInputs = basePath + "entries.csv";
        entryService.loadFromCsvFile(entriesInputs);
    }

}
