package wantsome.budget_tracker.database.dto;

import java.sql.Date;
import java.util.Objects;

public class Entry {

    private long id;
    private Date date;
    private String description;
    private double value;
    private TransactionType type;
    private int categoryId;

    public Entry(long id, Date date, String description, double value, TransactionType type, int categoryId) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.value = value;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Entry(Date date, String description, double value, TransactionType type, int categoryId) {
        this(-1, date, description, value, type, categoryId);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", value=" + value +
                ", type=" + type +
                ", category=" + categoryId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entry)) return false;
        Entry entry = (Entry) o;
        return id == entry.id &&
                Double.compare(entry.value, value) == 0 &&
                categoryId == entry.categoryId &&
                Objects.equals(date, entry.date) &&
                Objects.equals(description, entry.description) &&
                type == entry.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, description, value, type, categoryId);
    }

    public static Entry convertFromLine(String line) {
        String[] parts = line.split(",");
        return new Entry(
                Date.valueOf(parts[0].trim()),
                parts[1].trim(),
                Double.parseDouble(parts[2].trim()),
                TransactionType.valueOf(parts[3].trim()),
                Integer.parseInt(parts[4].trim()));
    }

}
