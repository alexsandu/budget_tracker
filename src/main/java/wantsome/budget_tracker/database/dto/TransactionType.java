package wantsome.budget_tracker.database.dto;

public enum TransactionType {
    INCOME, EXPENSE
}
