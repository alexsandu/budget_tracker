package wantsome.budget_tracker.database.dao;

import wantsome.budget_tracker.database.connection.DatabaseManager;
import wantsome.budget_tracker.database.dto.Entry;
import wantsome.budget_tracker.database.dto.TransactionType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EntryDao {

    private static EntryDao instance;
    private EntryDao() {
    }
    public static EntryDao getInstance() {
        if (instance == null) {
            instance = new EntryDao();
        }
        return instance;
    }

    private static final String TABLE_NAME = "ENTRIES";
    private static final String FLD_ID = "id";
    private static final String FLD_DATE = "transaction_date";
    private static final String FLD_DESCRIPTION = "description";
    private static final String FLD_VALUE = "value";
    private static final String FLD_TYPE = "type";
    private static final String FLD_CATEGORY_ID = "category_id";

    public void create(Entry entry) throws SQLException {
        String insertQuery = "insert into " + TABLE_NAME +
                " (" + FLD_DATE + ", " + FLD_DESCRIPTION + ", " + FLD_VALUE + ", " + FLD_TYPE + ", " + FLD_CATEGORY_ID + ")" +
                " values (strftime(?),?,?,?,?)";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setDate(1, entry.getDate());
            ps.setString(2, entry.getDescription());
            ps.setDouble(3, entry.getValue());
            ps.setString(4, entry.getType().name());
            ps.setInt(5, entry.getCategoryId());
            ps.executeUpdate();
        }
    }

    private Entry extractEntry(ResultSet rs) throws SQLException {
        int id = rs.getInt(FLD_ID);
        Date date = rs.getDate(FLD_DATE);
        String description = rs.getString(FLD_DESCRIPTION);
        double value = rs.getDouble(FLD_VALUE);
        TransactionType type = TransactionType.valueOf(rs.getString(FLD_TYPE));
        int category = rs.getInt(FLD_CATEGORY_ID);
        return new Entry(id, date, description, value, type, category);
    }

    public List<Entry> readAll() throws SQLException {
        List<Entry> entryList = new ArrayList<>();
        String selectQuery = "select * from " + TABLE_NAME;

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(selectQuery);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                entryList.add(extractEntry(rs));
            }
            return entryList;
        }
    }

    public Optional<Entry> readById(Long id) throws SQLException {
        String selectQuery = "select * from " + TABLE_NAME + " where " + FLD_ID + " = ?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(selectQuery)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(extractEntry(rs));
            }
        }
        return Optional.empty();
    }

    public List<Entry> readByDate(Date dateFrom, Date dateTo) throws SQLException {
        List<Entry> entryList = new ArrayList<>();
        String selectQuery = "select * from " + TABLE_NAME + " where " + FLD_DATE + " between ? and ?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(selectQuery)) {
            ps.setDate(1, dateFrom);
            ps.setDate(2, dateTo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                entryList.add(extractEntry(rs));
            }
            return entryList;
        }
    }

    public void update(Entry entry) throws SQLException {
        String updateQuery = "update " + TABLE_NAME +
                " set " + FLD_DATE + "=?, " + FLD_DESCRIPTION + "=?, " + FLD_VALUE + "=?, " + FLD_TYPE + "=?, " + FLD_CATEGORY_ID + "=? " +
                "where " + FLD_ID + "=?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setDate(1, entry.getDate());
            ps.setString(2, entry.getDescription());
            ps.setDouble(3, entry.getValue());
            ps.setString(4, entry.getType().name());
            ps.setInt(5, entry.getCategoryId());
            ps.setLong(6, entry.getId());
            ps.executeUpdate();
        }
    }

    public void deleteById(Long id) throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME + " where " + FLD_ID + "=?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setLong(1, id);
            ps.executeUpdate();
        }
    }

    public void deleteByDate(Date date) throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME + " where " + FLD_DATE + "= ?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setDate(1, date);
            ps.executeUpdate();
        }
    }

    public void deleteAll() throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME;

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.executeUpdate();
        }
    }

}
