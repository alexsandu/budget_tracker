package wantsome.budget_tracker.database.dao;

import wantsome.budget_tracker.database.connection.DatabaseManager;
import wantsome.budget_tracker.database.dto.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryDao {

    private static CategoryDao instance;
    private CategoryDao() {
    }
    public static CategoryDao getInstance() {
        if (instance == null) {
            instance = new CategoryDao();
        }
        return instance;
    }

    private static final String TABLE_NAME = "CATEGORIES";
    private static final String FLD_ID = "id";
    private static final String FLD_DESCRIPTION = "description";

    public void create(Category category) throws SQLException {
        String insertQuery = "insert into " + TABLE_NAME + " (" + FLD_DESCRIPTION + ")" + " values (?)";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(insertQuery)) {
            ps.setString(1, category.getDescription());
            ps.executeUpdate();
        }
    }

    private Category extractCategory(ResultSet rs) throws SQLException {
        int id = rs.getInt(FLD_ID);
        String description = rs.getString(FLD_DESCRIPTION);
        return new Category(id, description);
    }

    public List<Category> readAll() throws SQLException {
        List<Category> categoriesList = new ArrayList<>();
        String selectQuery = "select * from " + TABLE_NAME;

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(selectQuery);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                categoriesList.add(extractCategory(rs));
            }
            return categoriesList;
        }
    }

    public Optional<Category> readById(Integer id) throws SQLException {
        String selectQuery = "select * from " + TABLE_NAME + " where " + FLD_ID + " = ?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(selectQuery)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(extractCategory(rs));
            }
        }
        return Optional.empty();
    }

    public void update(Category category) throws SQLException {
        String updateQuery = "update " + TABLE_NAME + " set " + FLD_DESCRIPTION + "=?" + "where " + FLD_ID + "=?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setString(1, category.getDescription());
            ps.setInt(2, category.getId());
            ps.executeUpdate();
        }
    }

    public void deleteById(Integer id) throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME + " where " + FLD_ID + "=?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }

    public void deleteByDescription(String description) throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME + " where " + FLD_DESCRIPTION + "=?";

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setString(1, description);
            ps.executeUpdate();
        }
    }

    public void deleteAll() throws SQLException {
        String deleteQuery = "delete from " + TABLE_NAME;

        try (Connection connection = DatabaseManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.executeUpdate();
        }
    }

}

