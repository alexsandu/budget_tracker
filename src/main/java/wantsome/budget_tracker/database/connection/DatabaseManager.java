package wantsome.budget_tracker.database.connection;

import wantsome.budget_tracker.business.exceptions.DatabaseConnectionException;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseManager {
    public static Connection getConnection() {
        String url = "jdbc:sqlite:budgetTracker.db";
        try {
            return DataSourceConnection.getInstance().createConnection(url);
        } catch (SQLException e) {
            throw new DatabaseConnectionException(e.getMessage());
        }
    }

}
