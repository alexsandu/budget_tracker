package wantsome.budget_tracker.database.connection;

import org.sqlite.javax.SQLiteConnectionPoolDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceConnection {
    private static DataSourceConnection instance;

    private DataSourceConnection(){}
    public static DataSourceConnection getInstance(){
        if (instance == null) {
            instance = new DataSourceConnection();
        }
        return instance;
    }

    public Connection createConnection(String url) throws SQLException {
        DataSource dataSource = getDataSourceConnPool(url);
        return dataSource.getConnection();
    }

    private static DataSource getDataSourceConnPool(String url) {
        SQLiteConnectionPoolDataSource dataSource = new SQLiteConnectionPoolDataSource();
        dataSource.setUrl(url);
        dataSource.setEnforceForeignKeys(true); //enable foreign keys (disabled by default in SQLite)
        return dataSource;
    }

}
