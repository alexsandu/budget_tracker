package wantsome.budget_tracker.database.ddl;

import wantsome.budget_tracker.database.connection.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CategoryDdl {

    public static void createTable() {
        String sql = "create table if not exists CATEGORIES ( " +
                " id integer primary key autoincrement, " +
                " description varchar(100) not null UNIQUE);";

        try (Connection conn = DatabaseManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.out.println("Error while creating table: " + e.getMessage());
        }
    }

    public static void dropTable() {
        String sql = "drop table if exists CATEGORIES;";

        try (Connection conn = DatabaseManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.out.println("Error dropping table: " + e.getMessage());
        }
    }

}
