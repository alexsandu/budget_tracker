package wantsome.budget_tracker.database.ddl;

import wantsome.budget_tracker.database.connection.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.budget_tracker.database.dto.TransactionType.EXPENSE;
import static wantsome.budget_tracker.database.dto.TransactionType.INCOME;

public class EntryDdl {

    public static void createTable() {
        String sql = "create table if not exists ENTRIES ( " +
                " id integer primary key autoincrement, " +
                " transaction_date date not null, " +
                " description varchar(100) not null, " +
                " value real not null, " +
                " type varchar(10) check (type in ('" + INCOME + "', '" + EXPENSE + "')) not null, " +
                " category_id integer not null references CATEGORIES(id));";

        try (Connection conn = DatabaseManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.out.println("Error creating table: " + e.getMessage());
        }
    }

    public static void dropTable() {
        String sql = "drop table if exists ENTRIES;";

        try (Connection conn = DatabaseManager.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.out.println("Error dropping table: " + e.getMessage());
        }
    }

}
