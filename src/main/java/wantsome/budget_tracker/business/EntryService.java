package wantsome.budget_tracker.business;

import wantsome.budget_tracker.database.dao.EntryDao;
import wantsome.budget_tracker.database.dto.Category;
import wantsome.budget_tracker.database.dto.Entry;
import wantsome.budget_tracker.database.dto.TransactionType;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;

public class EntryService {
    private EntryDao entryDao;

    private static EntryService instance;
    public static EntryService getInstance() {
        if (instance == null) {
            instance = new EntryService();
        }
        return instance;
    }
    private EntryService() {
        entryDao = EntryDao.getInstance();
    }

    public void create(Entry entry) {
        try {
            entryDao.create(entry);
        } catch (SQLException e) {
            System.out.println("Error in inserting entry: " + e.getMessage());
        }
    }

    public Optional<Entry> readById(long id) {
        try {
            return entryDao.readById(id);
        } catch (SQLException e) {
            System.out.println("Error in reading entry by id: " + e.getMessage());
        }
        return Optional.empty();
    }

    public List<Entry> readAll() {
        try {
            return entryDao.readAll();
        } catch (SQLException e) {
            System.out.println("Error in reading all entries: " + e.getMessage());
        }
        return new ArrayList<>();
    }

    public List<Entry> readByDate(Date dateFrom, Date dateTo) {
        try {
            return entryDao.readByDate(dateFrom, dateTo);
        } catch (SQLException e) {
            System.out.println("Error in reading entry by date: " + e.getMessage());
        }
        return new ArrayList<>();
    }

    public void update(Entry entry) {
        try {
            entryDao.update(entry);
        } catch (SQLException e) {
            System.out.println("Error in updating entry: " + e.getMessage());
        }
    }

    public void deleteById(long id) {
        try {
            entryDao.deleteById(id);
        } catch (SQLException e) {
            System.out.println("Error in deleting entry by id: " + e.getMessage());
        }
    }

    public void deleteByDate(Date date) {
        try {
            entryDao.deleteByDate(date);
        } catch (SQLException e) {
            System.out.println("Error in deleting entry by date: " + e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            entryDao.deleteAll();
        } catch (SQLException e) {
            System.out.println("Error in deleting all entries: " + e.getMessage());
        }
    }

    public void loadFromCsvFile(String fileName) {
        try {

            Scanner sc = new Scanner(new File(fileName));
            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                try {
                    entryDao.create(Entry.convertFromLine(line));
                } catch (Exception e) {
                    System.err.println("Skipped invalid line: '" + line + "', error was: " + e);
                }
            }
            System.out.println("ENTRY table updated from " + fileName);
        } catch (FileNotFoundException e) {
            System.out.println("File not found, error: " + e.getMessage());
        }
    }

    public double balance() {
        return readAll().stream()
                .mapToDouble(i -> i.getType() == TransactionType.EXPENSE ? i.getValue() * (-1) : i.getValue())
                .sum();
    }

    public Map<String, String> getExpensesStats(List<Entry> entries, List<Category> categories) {
        Map<String, String> stats = new LinkedHashMap<>();
        for (Category cat : categories) {
            double finalValue = 0;
            for (Entry e : entries) {
                if (e.getCategoryId() == cat.getId() && e.getType().equals(TransactionType.EXPENSE)) {
                    finalValue += e.getValue();
                    stats.put(cat.getDescription(), String.valueOf(finalValue));
                }
            }
        }
        return stats;
    }

    public Map<String, String> getIncomesStats(List<Entry> entries, List<Category> categories) {
        Map<String, String> stats = new LinkedHashMap<>();
        for (Category cat : categories) {
            double finalValue = 0;
            for (Entry e : entries) {
                if (e.getCategoryId() == cat.getId() && e.getType().equals(TransactionType.INCOME)) {
                    finalValue += e.getValue();
                    stats.put(cat.getDescription(), String.valueOf(finalValue));
                }
            }
        }
        return stats;
    }

    public Map<String, String> getTotalExpensesStats(List<Entry> entries) {
        Map<String, String> stats = new LinkedHashMap<>();
            double finalValue = 0;
            for (Entry e : entries) {
                if (e.getType().equals(TransactionType.EXPENSE)) {
                    finalValue += e.getValue();
                }
            }
            stats.put("Total Expenses", String.valueOf(finalValue));
        return stats;
    }

    public Map<String, String> getTotalIncomesStats(List<Entry> entries) {
        Map<String, String> stats = new LinkedHashMap<>();
        double finalValue = 0;
        for (Entry e : entries) {
            if (e.getType().equals(TransactionType.INCOME)) {
                finalValue += e.getValue();
            }
        }
        stats.put("Total Incomes", String.valueOf(finalValue));
        return stats;
    }
}
