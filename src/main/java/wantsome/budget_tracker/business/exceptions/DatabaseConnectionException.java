package wantsome.budget_tracker.business.exceptions;

public class DatabaseConnectionException extends RuntimeException {
    public DatabaseConnectionException(String errorMsg){
        System.out.println("Error in creating connection: " + errorMsg);
    }
}
