package wantsome.budget_tracker.business;

import wantsome.budget_tracker.database.dao.CategoryDao;
import wantsome.budget_tracker.database.dto.Category;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class CategoryService {
    private CategoryDao categoryDao;

    private static CategoryService instance;

    public static CategoryService getInstance() {
        if (instance == null) {
            instance = new CategoryService();
        }
        return instance;
    }

    private CategoryService() {
        categoryDao = CategoryDao.getInstance();
    }

    public void create(Category category) {
        try {
            categoryDao.create(category);
        } catch (SQLException e) {
            System.out.println("Error in inserting category: " + e.getMessage());
        }
    }

    public Optional<Category> readById(Integer id) {
        try {
            return categoryDao.readById(id);
        } catch (SQLException e) {
            System.out.println("Error in reading category by id: " + e.getMessage());
        }
        return Optional.empty();
    }

    public List<Category> readAll() {
        try {
            return categoryDao.readAll();
        } catch (SQLException e) {
            System.out.println("Error in reading all categories: " + e.getMessage());
        }
        return new ArrayList<>();
    }

    public void update(Category category) {
        try {
            categoryDao.update(category);
        } catch (SQLException e) {
            System.out.println("Error in updating category: " + e.getMessage());
        }
    }

    public void deleteById(Integer id) {
        try {
            categoryDao.deleteById(id);
        } catch (SQLException e) {
            System.out.println("Error in deleting category by id: " + e.getMessage());
        }
    }

    public void deleteByDescription(String description) {
        try {
            categoryDao.deleteByDescription(description);
        } catch (SQLException e) {
            System.out.println("Error in deleting category by description: " + e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            categoryDao.deleteAll();
        } catch (SQLException e) {
            System.out.println("Error in deleting all categories: " + e.getMessage());
        }
    }

    public void loadFromCsvFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                try {
                    categoryDao.create(Category.convertFromLine(line));
                } catch (Exception e) {
                    System.err.println("Skipped invalid line: '" + line + "', error was: " + e);
                }
            }
            System.out.println("CATEGORY table updated from " + fileName);
        } catch (FileNotFoundException e) {
            System.out.println("File not found, error: " + e);
        }
    }

}
