package wantsome.budget_tracker.presentation.ui_controllers;

import spark.Request;
import wantsome.budget_tracker.business.CategoryService;
import wantsome.budget_tracker.business.EntryService;
import wantsome.budget_tracker.database.dto.Entry;
import wantsome.budget_tracker.database.dto.TransactionType;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static wantsome.budget_tracker.presentation.SparkUtil.render;

public class MainEntryController {
    private static EntryService entryService = EntryService.getInstance();
    private static CategoryService categoryService = CategoryService.getInstance();

    private static List<Entry> filteredList = entryService.readAll();
    private static String sortBy = "ID";
    private static boolean sortAsc = true;
    private static Date dateFrom;
    private static Date dateTo;
    private static String filteredType;

    public static String showMainPage() {
        Map<String, Object> model = new HashMap<>();
        return render(model, "budget_tracker/showEntries.vm");
    }

    public static String showEntries(Request req) {
        String sort = req.queryParams("sortBy");
        readSortParams(req);
        Map<String, Object> model = new HashMap<>();
        if (sort == null) {
            model.put("entries", entryService.readAll());
            sortBy = "ID";
            sortAsc = true;
            dateTo = null;
            dateFrom = null;
            filteredType = "ALL";
            filteredList.clear();
            filteredList.addAll(entryService.readAll());
        } else {
            model.put("entries", getSortedEntries());
        }
        model.put("categories", categoryService.readAll());
        model.put("types", TransactionType.values());
        model.put("error", "");
        model.put("sortBy", sortBy);
        model.put("sortAsc", sortAsc);
        model.put("dateFrom", dateFrom);
        model.put("dateTo", dateTo);
        model.put("filteredType", filteredType);
        return render(model, "budget_tracker/showEntries.vm");
    }

    private static List<Entry> getSortedEntries() {
        Comparator<Entry> comparator;
        switch (sortBy) {
            case "Date":
                comparator = Comparator.comparing(Entry::getDate);
                break;
            case "Value":
                comparator = Comparator.comparing(Entry::getValue);
                break;
            default:
                comparator = Comparator.comparing(Entry::getId);
                break;
        }
        if (!sortAsc) {
            comparator = comparator.reversed();
        }
        return filteredList.stream().sorted(comparator).collect(toList());
    }

    private static void readSortParams(Request req) {
        String newSortBy = req.queryParams("sortBy");
        if (newSortBy != null) {
            sortAsc = !newSortBy.equals(sortBy) || !sortAsc;
            sortBy = newSortBy;
        }
    }

    public static Object handleFilterByDateOrTypeRequest(Request req) {
        Map<String, Object> model = new HashMap<>();
        try {
            dateFrom = testDateFrom(req.queryParams("dateFrom"));
            dateTo = testDateTo(req.queryParams("dateTo"));
            filteredType = req.queryParams("type");
            List<Entry> readByDate = filterByDate(entryService.readAll(), dateFrom, dateTo);
            List<Entry> readByType = filterByType(readByDate, filteredType);
            filteredList.clear();
            filteredList.addAll(readByType);
            sortBy = "ID";
            sortAsc = true;
            model.put("error", readByDate.isEmpty() ? "No records registered between the selected timeframe!" : "");
            model.put("entries", readByType);
            model.put("categories", categoryService.readAll());
            model.put("types", TransactionType.values());
            model.put("sortBy", sortBy);
            model.put("sortAsc", sortAsc);
            model.put("dateFrom", dateFrom);
            model.put("dateTo", dateTo);
            model.put("filteredType", filteredType);
            return render(model, "budget_tracker/showEntries.vm");
        } catch (Exception e) {
            sortBy = "ID";
            sortAsc = true;
            model.put("error", e.getMessage());
            model.put("entries", entryService.readAll());
            model.put("categories", categoryService.readAll());
            model.put("types", TransactionType.values());
            model.put("sortBy", sortBy);
            model.put("sortAsc", sortAsc);
            return render(model, "budget_tracker/showEntries.vm");
        }
    }

    private static Date testDateTo(String date) {
        if (date == null || date.isEmpty()) {
            date = String.valueOf(LocalDate.now());
        }
        return Date.valueOf(date);
    }

    private static Date testDateFrom(String date) {
        if (date == null || date.isEmpty()) {
            date = String.valueOf(entryService.readAll().stream()
                    .map(Entry::getDate)
                    .min(Date::compareTo).orElse(null));
        }
        return Date.valueOf(date);
    }

    public static List<Entry> filterByType(List<Entry> entries, String type) {
        if (type == null || type.isEmpty()) {
            throw new RuntimeException("Type can not be empty!");
        }
        if (type.equals("ALL")) {
            return entries;
        }
        return entries.stream()
                .filter(e -> String.valueOf(e.getType()).equals(type))
                .collect(Collectors.toList());
    }

    public static List<Entry> filterByDate(List<Entry> entries, Date dateFrom, Date dateTo) {
        return entries.stream()
                .filter(e -> (e.getDate().after(dateFrom) || e.getDate().equals(dateFrom))
                        && (e.getDate().before(dateTo) || e.getDate().equals(dateTo)))
                .collect(Collectors.toList());
    }

}
