package wantsome.budget_tracker.presentation.ui_controllers;

import spark.Request;
import spark.Response;
import wantsome.budget_tracker.business.CategoryService;
import wantsome.budget_tracker.business.EntryService;
import wantsome.budget_tracker.database.dto.Entry;
import wantsome.budget_tracker.database.dto.TransactionType;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.budget_tracker.presentation.SparkUtil.render;

public class AddUpdateDeleteEntryController {
    private static EntryService entryService = EntryService.getInstance();
    private static CategoryService categoryService = CategoryService.getInstance();

    public static String addEntriesPage() {
        Map<String, Object> model = new HashMap<>();
        model.put("error", "");
        model.put("categories", categoryService.readAll());
        model.put("types", TransactionType.values());
        return render(model, "budget_tracker/addEntries.vm");
    }

    public static Object handleAddEntryRequest(Request req, Response res) {
        try {
            Entry entry = addEntry(
                    req.queryParams("id"),
                    req.queryParams("date"),
                    req.queryParams("description"),
                    req.queryParams("value"),
                    req.queryParams("type"),
                    req.queryParams("category"));
            entryService.create(entry);
            res.redirect("/budgetTracker/showEntries");
            return res;
        } catch (Exception e) {
            Map<String, Object> model = new HashMap<>();
            model.put("categories", categoryService.readAll());
            model.put("types", TransactionType.values());
            model.put("error", e.getMessage());
            return render(model, "budget_tracker/addEntries.vm");
        }
    }

    private static Entry addEntry(String id, String date, String description, String value, String type, String category) {
        if (date == null || date.isEmpty()) {
            throw new RuntimeException("Date can not be empty!");
        }
        Date dateD;
        try {
            dateD = Date.valueOf(date);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Date is not in the right format! yyyy-mm-dd");
        }
        if (description == null || description.isEmpty()) {
            throw new RuntimeException("Description can not be empty!");
        }
        if (value == null || value.isEmpty()) {
            throw new RuntimeException("Value can not be empty!");
        }
        double valueDoub;
        try {
            valueDoub = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Value must be an integer number or decimal!");
        }
        if (type == null || type.isEmpty()) {
            throw new RuntimeException("Type can not be empty!");
        }
        return new Entry(
                dateD,
                description,
                valueDoub,
                TransactionType.valueOf(type),
                Integer.parseInt(category));
    }

    public static String updateEntriesPage(Request req, String err) {
        long idParam = Long.parseLong(req.params("id"));
        Entry entry = null;
        String desc = "";
        Date date = null;
        double value = 0;
        String entryType = "";
        int categoryId = 0;

        Optional<Entry> e = entryService.readById(idParam);
        if (e.isPresent()) {
            entry = e.get();
            date = e.get().getDate();
            desc = e.get().getDescription();
            value = e.get().getValue();
            entryType = String.valueOf(e.get().getType());
            categoryId = e.get().getCategoryId();
        }

        Map<String, Object> model = new HashMap<>();
        model.put("error", err);
        model.put("selectedId", idParam);
        model.put("selectedEntry", entry);
        model.put("selectedDate", date);
        model.put("selectedDesc", desc);
        model.put("selectedValue", value);
        model.put("types", TransactionType.values());
        model.put("entryType", entryType);
        model.put("categories", categoryService.readAll());
        model.put("categoryId", categoryId);
        return render(model, "budget_tracker/updateEntries.vm");
    }

    public static Object handleUpdateEntryRequest(Request req, Response res) {
        try {
            Entry entry = updateEntry(
                    req.params("id"),
                    req.queryParams("date"),
                    req.queryParams("description"),
                    req.queryParams("value"),
                    req.queryParams("type"),
                    req.queryParams("category"));
            entryService.update(entry);
            res.redirect("/budgetTracker/showEntries");
            return res;
        } catch (Exception e) {
            return updateEntriesPage(req, e.getMessage());
        }
    }

    private static Entry updateEntry(String id, String date, String description, String value, String type, String category) {
        if (date == null || date.isEmpty()) {
            throw new RuntimeException("Date can not be empty!");
        }
        Date dateD;
        try {
            dateD = Date.valueOf(date);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Date is not in the right format! yyyy-mm-dd");
        }
        if (description == null || description.isEmpty()) {
            throw new RuntimeException("Description can not be empty!");
        }
        if (value == null || value.isEmpty()) {
            throw new RuntimeException("Value can not be empty!");
        }
        double valueDoub;
        try {
            valueDoub = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Value must be an integer number or decimal!");
        }
        return new Entry(
                Long.parseLong(id),
                dateD,
                description,
                valueDoub,
                TransactionType.valueOf(type),
                Integer.parseInt(category));
    }

    public static Response handleDeleteEntryRequest(Request req, Response res) {
        long idParam = Long.parseLong(req.params("id"));
        entryService.deleteById(idParam);
        res.redirect("/budgetTracker/showEntries");
        return res;
    }

}
