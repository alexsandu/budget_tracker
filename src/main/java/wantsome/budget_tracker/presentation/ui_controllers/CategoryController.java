package wantsome.budget_tracker.presentation.ui_controllers;

import spark.Request;
import spark.Response;
import wantsome.budget_tracker.business.CategoryService;
import wantsome.budget_tracker.business.EntryService;
import wantsome.budget_tracker.database.dto.Entry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.budget_tracker.presentation.SparkUtil.render;

public class CategoryController {
    private static EntryService entryService = EntryService.getInstance();
    private static CategoryService categoryService = CategoryService.getInstance();

    public static String showCategories(String err) {
        Map<String, Object> model = new HashMap<>();
        model.put("categories", categoryService.readAll());
        model.put("error", err);
        return render(model, "budget_tracker/showCategories.vm");
    }

    public static String addCategoriesPage() {
        Map<String, Object> model = new HashMap<>();
        model.put("error", "");
        return render(model, "budget_tracker/addCategories.vm");
    }

    public static Object handleAddCategoryRequest(Request req, Response res) {
        try {
            wantsome.budget_tracker.database.dto.Category categories = addCategory(
                    req.queryParams("id"),
                    req.queryParams("description"));
            categoryService.create(categories);
            res.redirect("/budgetTracker/showCategories");
            return res;
        } catch (Exception e) {
            Map<String, Object> model = new HashMap<>();
            model.put("error", e.getMessage());
            return render(model, "budget_tracker/addCategories.vm");
        }
    }

    private static wantsome.budget_tracker.database.dto.Category addCategory(String id, String description) {
        if (description == null || description.isEmpty()) {
            throw new RuntimeException("Description can not be empty!");
        }
        for (wantsome.budget_tracker.database.dto.Category cat : categoryService.readAll()) {
            if (cat.getDescription().equals(description)) {
                throw new RuntimeException("Error in inserting category: description should be UNIQUE!");
            }
        }
        return new wantsome.budget_tracker.database.dto.Category(
                description);
    }

    public static String updateCategoriesPage(Request req, String err) {
        int idParam = Integer.parseInt(req.params("id"));
        String desc = "";
        wantsome.budget_tracker.database.dto.Category categ = null;
        Optional<wantsome.budget_tracker.database.dto.Category> cat = categoryService.readById(idParam);
        if (cat.isPresent()) {
            categ = cat.get();
            desc = cat.get().getDescription();
        }
        Map<String, Object> model = new HashMap<>();
        model.put("error", err);
        model.put("selectedId", idParam);
        model.put("selectedCat", categ);
        model.put("selectedDesc", desc);
        return render(model, "budget_tracker/updateCategories.vm");
    }

    public static Object handleUpdateCategoryRequest(Request req, Response res) {
        try {
            wantsome.budget_tracker.database.dto.Category categories = updateCategory(
                    req.params("id"),
                    req.queryParams("description"));
            categoryService.update(categories);
            res.redirect("/budgetTracker/showCategories");
            return res;
        } catch (Exception e) {
            return updateCategoriesPage(req, e.getMessage());
        }
    }

    private static wantsome.budget_tracker.database.dto.Category updateCategory(String id, String description) {
        if (description == null || description.isEmpty()) {
            throw new RuntimeException("Description can not be empty!");
        }
        return new wantsome.budget_tracker.database.dto.Category(
                Integer.parseInt(id),
                description);
    }

    public static Object handleDeleteCategoryRequest(Request req, Response res) {
        int idParam = Integer.parseInt(req.params("id"));
        try {
            validateCategoryRemoval(idParam);
            categoryService.deleteById(idParam);
            res.redirect("/budgetTracker/showCategories");
            return res;
        } catch (Exception e) {
            return showCategories(e.getMessage());
        }
    }

    private static void validateCategoryRemoval(int id) {
        for (Entry e : entryService.readAll()) {
            if (e.getCategoryId() == id) {
                throw new RuntimeException("Could not delete category " + e.getCategoryId() + " because it is already used in entries list!");
            }
        }
    }

}
