package wantsome.budget_tracker.presentation.ui_controllers;

import wantsome.budget_tracker.business.CategoryService;
import wantsome.budget_tracker.business.EntryService;

import java.util.HashMap;
import java.util.Map;

import static wantsome.budget_tracker.presentation.SparkUtil.render;

public class ReportsController {
    private static EntryService entryService = EntryService.getInstance();
    private static CategoryService categoryService = CategoryService.getInstance();

    public static String analyticsPage() {
        double balance = entryService.balance();
        Map<String, String> expensesStats = new HashMap<>(entryService.getExpensesStats(entryService.readAll(), categoryService.readAll()));
        Map<String, String> incomesStats = new HashMap<>(entryService.getIncomesStats(entryService.readAll(), categoryService.readAll()));
        Map<String, String> totalIncomesStats = new HashMap<>(entryService.getTotalIncomesStats(entryService.readAll()));
        Map<String, String> totalExpensesStats = new HashMap<>(entryService.getTotalExpensesStats(entryService.readAll()));
        Map<String, Object> model = new HashMap<>();
        model.put("balance", balance);
        model.put("entriesNr", String.valueOf(entryService.readAll().size()));
        model.put("totalExpStats", totalExpensesStats);
        model.put("totalIncStats", totalIncomesStats);
        model.put("expensesStats", expensesStats);
        model.put("incomesStats", incomesStats);
        return render(model, "budget_tracker/reports.vm");
    }

}
