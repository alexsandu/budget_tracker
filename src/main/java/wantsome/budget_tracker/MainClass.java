package wantsome.budget_tracker;

import wantsome.budget_tracker.presentation.ui_controllers.AddUpdateDeleteEntryController;
import wantsome.budget_tracker.presentation.ui_controllers.CategoryController;
import wantsome.budget_tracker.presentation.ui_controllers.MainEntryController;

import static spark.Spark.*;
import static wantsome.budget_tracker.database.init.DbInit.initializeDemoDb;
import static wantsome.budget_tracker.presentation.ui_controllers.AddUpdateDeleteEntryController.*;
import static wantsome.budget_tracker.presentation.ui_controllers.MainEntryController.*;
import static wantsome.budget_tracker.presentation.ui_controllers.CategoryController.*;
import static wantsome.budget_tracker.presentation.ui_controllers.ReportsController.*;

public class MainClass {
    public static void main(String[] args) {
        initializeDemoDb();
        staticFiles.location("/budget_tracker/public");

        get("/budgetTracker", (req, res) -> showMainPage());

        get("/budgetTracker/showEntries", (req, res) -> showEntries(req));
        post("/budgetTracker/showEntries", (req, res) -> MainEntryController.handleFilterByDateOrTypeRequest(req));

        get("/budgetTracker/showCategories", (req, res) -> showCategories(""));

        get("/budgetTracker/addEntry", (req, res) -> addEntriesPage());
        post("/budgetTracker/addEntry", AddUpdateDeleteEntryController::handleAddEntryRequest);

        get("/budgetTracker/addCategory", (req, res) -> addCategoriesPage());
        post("/budgetTracker/addCategory", CategoryController::handleAddCategoryRequest);

        get("/budgetTracker/deleteEntry/:id", AddUpdateDeleteEntryController::handleDeleteEntryRequest);
        get("/budgetTracker/deleteCategory/:id", CategoryController::handleDeleteCategoryRequest);

        get("/budgetTracker/updateCategory/:id", (req, res) -> updateCategoriesPage(req, ""));
        post("/budgetTracker/updateCategory/:id", CategoryController::handleUpdateCategoryRequest);

        get("/budgetTracker/updateEntry/:id", (req, res) -> updateEntriesPage(req, ""));
        post("/budgetTracker/updateEntry/:id", AddUpdateDeleteEntryController::handleUpdateEntryRequest);

        get("/budgetTracker/reports", (req, res) -> analyticsPage());

        System.out.println("server started: http://localhost:4567/budgetTracker");
    }

}
