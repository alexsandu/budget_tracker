package wantsome.budget_tracker;

import org.junit.*;
import wantsome.budget_tracker.database.dto.Category;
import wantsome.budget_tracker.database.dto.Entry;
import wantsome.budget_tracker.database.dto.TransactionType;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static wantsome.budget_tracker.database.init.DbInit.*;

public class DatabaseTest {

    private static final List<Entry> sampleEntries = Arrays.asList(
            new Entry(Date.valueOf("2020-01-09"), "test1", 100, TransactionType.EXPENSE, 3),
            new Entry(Date.valueOf("2020-01-10"), "test2", 50, TransactionType.EXPENSE, 4),
            new Entry(Date.valueOf("2020-01-11"), "test3", 25, TransactionType.EXPENSE, 5));

    private static final List<Category> sampleCategories = Arrays.asList(
            new Category("Vacation"),
            new Category("Test"));

    @BeforeClass
    public static void initDbBeforeAnyTests() {
        initializeDemoDb();
    }

    @AfterClass
    public static void deleteAllById() {
        for (Entry entry : entryService.readAll()) {
            entryService.deleteById(entry.getId());
        }
        for (Category c : categoryService.readAll()) {
            categoryService.deleteById(c.getId());
        }
        assertEquals(0, entryService.readAll().size());
        assertEquals(0, categoryService.readAll().size());
    }

    @Test
    public void readEntries() {
        assertEquals(9, entryService.readAll().size());
    }

    @Test
    public void readCategories() {
        assertEquals(6, categoryService.readAll().size());
    }

    @Test
    public void insertDeleteSampleEntries() {
        for (Entry entry : sampleEntries) {
            entryService.create(entry);
        }
        assertEquals(12, entryService.readAll().size());
        for (Entry entry : sampleEntries) {
            entryService.deleteByDate(entry.getDate());
        }
        assertEquals(9, entryService.readAll().size());
    }

    @Test
    public void insertDeleteSampleCategories() {
        for (Category cat : sampleCategories) {
            categoryService.create(cat);
        }
        assertEquals(8, categoryService.readAll().size());
        for (Category c : sampleCategories) {
            categoryService.deleteByDescription(c.getDescription());
        }
        assertEquals(6, categoryService.readAll().size());
    }

    @Test
    public void readEntryByDate() {
        Date dateFrom = Date.valueOf("2019-11-01");
        Date dateTo = Date.valueOf("2019-11-30");
        assertEquals(3, entryService.readByDate(dateFrom, dateTo).size());
    }

    @Test
    public void readEntryById() {
        Entry entry = entryService.readAll().get(8);
        assertEquals(entry, entryService.readById(entry.getId()).get());
    }

    @Test
    public void readCategoryById() {
        Category cat = categoryService.readAll().get(5);
        assertEquals(cat, categoryService.readById(cat.getId()).get());
    }

    @Test
    public void readByInvalidId() {
        assertFalse(entryService.readById(22).isPresent());
        assertFalse(categoryService.readById(-22).isPresent());
    }

    @Test
    public void updateEntry() {
        Entry entry = entryService.readAll().get(8);
        Entry updateEntry = new Entry(entry.getId(), entry.getDate(), "Updated", entry.getValue(), entry.getType(), entry.getCategoryId());
        entryService.update(updateEntry);
        assertEquals(updateEntry, entryService.readAll().get(8));
    }

    @Test
    public void updateCategory() {
        Category cat = categoryService.readAll().get(5);
        Category updateCategory = new Category(cat.getId(), "Updated");
        categoryService.update(updateCategory);
        assertEquals(updateCategory, categoryService.readAll().get(5));
    }

}