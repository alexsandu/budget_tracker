# Wantsome - Project Budget Tracker


## NOTES:

 - This is just a __template__ for the final project (will be completed at the
   end of the project).  
   It's just a basic working gradle project, with a default folder structure, including some common libraries.
  
   Final notes: 
   - short description of the __purpose and scope__ of the project
   - __initial setup instructions__ (init db, others)
   - __usage instructions__
   - __technical details__ about how it is built (libs used, db, etc)
   
   For formatting __MarkDown syntax__ can be used: 
   https://guides.github.com/features/mastering-markdown/
________________________________________________________________________________
