## Planificare proiect final

__Lucuri de discutat/planificat__ la inceputul proiectului:

#### 0) Idea de baza:

  De definit:
  - __scopul aplicatie__, idea de baza (scurt, concis).
  - trasat __limitele__: 
    - ce e obligatoriu
    - ce e 'nice to have'
    - ce nu facem

#### 1) Reprezentarea datelor:

  De ce DATE are nevoie aplicatia (tipuri entitati), si cum ar fi reprezentate:
  - __clase Java__ (fielduri), si relatiile dintre ele (mostenire/compozitie..)
  - __tabele in DB__ (cate tabele, ce fielduri si de ce tipuri, ce relatii 
    intre tabele, ce alte constrainturi pe fielduri)


#### 2) Functionalitatea aplicatiei, scenarii:

  Ce ACTIUNI ar trebui sa poti face cu datele:
  - actiunile de baza suportate de aplicatie (inserare, actualizare, stergere, cautare?)
  - scenarii de folosire, d.p.d.v al userilor aplicatiei (sunt mai multe tipuri de useri?
    ce poate face fiecare?..)


#### 3) UI folosit
  
  Ce stil de UI vom folosi?
  - in mod text, interactiv (cam basic, realizabil)
  - stil web (java backend+also frontend)
  - standalone, cu JavaFX
  - fara UI, doar backend + teste demonstrative pt actiuni


#### 4) ORGANIZARE proiect:
  
  - De stabilit prioritati, ordine, impartirea muncii in echipa
________________________________________________________________________________
